<?php 
defined('BASEPATH') OR exit('No direct script access allowed!');

if (!function_exists('logger')) {
    function logger($met="", $sql="", $old="") {
        $ci =& get_instance();

        $u = $ci->session->userdata();

        if (!empty($old) && is_array($old))
            $old = json_encode($old);

        $user_info = array(
            'action_made' => json_encode(array(
                'method_used' => $met,
                'query_used' => $sql,
                'data_before' => $old 
            )),
            'user' => json_encode(array(
                'user_info' => array(
                    'emp_id' => $u['logged_in']['id'],
                    'user_name' => $u['logged_in']['username'],
                    'first_name' => $u['logged_in']['firstname'],
                    'last_name' => $u['logged_in']['lastname'],
                ),
                'user_sess' => array(
                    'session_id' => $u['sess_info']['session_id'],
                    'ip_address' => $u['sess_info']['ip_address'],
                    'user_agent' => $u['sess_info']['user_agent']
                )
            ))
        );

        load_db('logs');
        $ci->db->insert('user_logs', $user_info);

    }
}


?>