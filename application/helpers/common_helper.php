<?php 
defined('BASEPATH') OR exit('No direct script access allowed!');

if(!function_exists('pre_r')) {
	function pre_r($array) {
		if(is_array($array) || is_object($array))
		{
			echo'<pre>';
			print_r($array);
			echo'</pre>';
		}
		else
		{
			echo $array.' is not an array!';
		}
	}
}

if (!function_exists('load_db')) {
	function load_db($dbname) {
		$ci =& get_instance();

		$ci->db->close();

		// pre_r($ci->db); exit;

		/*if (!empty($dbname)) 
			return $ci->db = $ci->load->database($dbname, TRUE);
		else 
			echo "Error! load_db function must have a parameter"; exit;*/

		if (!empty($dbname)) {
			$ci->db = $ci->load->database($dbname, TRUE);
			if (connection_aborted() != 0) {
				// load_db($dbname);
				echo "CONNECTION FOR '".$dbname."' HAS BEEN ABBORTED! SHIT!";
				exit;
			}
			else { 
				return $ci->db;
			}
		} 
		else {
			echo "Error! load_db function must have a parameter"; exit;
		}
	}
}

if (!function_exists('local_ip')) {
	function local_ip() {
		$ip = null;
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}

if (!function_exists('client_ip')) {
	function client_ip() {
		$ip = getHostByName(php_uname('n'));
		return $ip;
	}
}

?>