  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Monitoring
        <small>Duplicate Barcode Checker</small>
      </h1>
    </section>

    <section class="content" style="min-height: 150px;">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">

            <div class="box-body">
              <form action="<?php echo base_url(); ?>" method="POST">
                  <div class="col-md-2"></div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="control-label">Barcode</label>
                      <input type="text" name="barcode" class="form-control" value="<?php echo (isset($barcode) ? $barcode : '') ?>">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="control-label">Select Branch</label>
                      <select name="branch" class="form-control">
                        <?php 
                          if (isset($branches)) {
                            $selected = '';
                            // echo '<option>Select a branch</option>';
                            foreach ($branches as $key => $value) {
                              if (isset($branch)) {
                                if ($branch == $key) {
                                  $selected = 'selected="selected"';}
                                else {
                                  $selected = '';
                                }
                              }
                              
                              echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                            }
                          } 
                          else {
                            echo "<option>No Branches Loaded!</option>";
                          }

                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-2">
                    <div class="form-group">
                     <label class="control-label"></label>
                      <input type="submit" class="btn btn-danger form-control" value="Search" style="margin-top: 5px;">
                    </div>  
                  </div>
                  <div class="col-md-2"></div>
              </form>
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <?php if (isset($row_main) && !empty($row_main)) { ?>
    <section class="content" style="min-height: 150px;">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo (isset($branch_name) ? 'MAIN - '.$branch_name: ''); ?></h3>
              <div class="pull-right">
                <?php if (isset($barcode)) { ?>
                <p>Download as Excel File &nbsp;<a href="<?php echo base_url('monitoring/get_excel/MAIN/'.$branch_name.'/main_'.$dbname.'/'.$barcode.''); ?>"><i class="fa fa-download"></i></a></p>
                <?php } else { ?>
                <p>Download as Excel File &nbsp;<a href="<?php echo base_url('monitoring/get_excel/MAIN/'.$branch_name.'/main_'.$dbname.''); ?>"><i class="fa fa-download"></i></a></p>
                <?php } ?>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-bordered table-striped">
                <tbody><tr>
                  <th>Product ID</th>
                  <th>Product Code</th>
                  <th>Barcode</th>
                  <th>Price Mode Code</th>
                  <th>Description</th>
                  <th>UOM</th>
                  <th>QTY</th>
                  <th>Markup</th>
                  <th>SRP</th>
                  <th>Last Date Modified</th>
                  <!-- <th style="width: 40px">Label</th> -->
                </tr>
                <?php
                  if (isset($row_main)) {
                    foreach ($row_main as $key => $value) {
                      echo '<tr>';
                        echo '<td>'.$value['ProductID'].'</td>';
                        echo '<td>'.$value['ProductCode'].'</td>';
                        echo '<td style="text-align: center;">'.$value['Barcode'].'</td>';
                        echo '<td>'.$value['PriceModeCode'].'</td>';
                        echo '<td>'.$value['Description'].'</td>';
                        echo '<td>'.$value['uom'].'</td>';
                        echo '<td>'.$value['qty'].'</td>';
                        echo '<td>'.$value['markup'].'</td>';
                        echo '<td>'.$value['srp'].'</td>';
                        echo '<td>'.$value['LastDateModified'].'</td>';
                      echo '</tr>';
                    }
                  }
                ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <!-- <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li> -->
              </ul>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <?php } else { ?>
    <?php } ?>

    <?php if (isset($row_branch) && !empty($row_branch)) { ?>
    <section class="content" style="min-height: 150px;">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo (isset($branch_name) ? 'BRANCH - '.$branch_name: ''); ?></h3>
              <div class="pull-right">
                <?php if (isset($barcode)) { ?>
                <p>Download as Excel File &nbsp;<a href="<?php echo base_url('monitoring/get_excel/BRANCH/'.$branch_name.'/'.$dbname.'/'.$barcode.''); ?>"><i class="fa fa-download"></i></a></p>
                <?php } else { ?>
                <p>Download as Excel File &nbsp;<a href="<?php echo base_url('monitoring/get_excel/BRANCH/'.$branch_name.'/'.$dbname.''); ?>"><i class="fa fa-download"></i></a></p>
                <?php } ?>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-bordered table-striped">
                <tbody><tr>
                  <th>Product ID</th>
                  <th>Product Code</th>
                  <th>Barcode</th>
                  <th>Price Mode Code</th>
                  <th>Description</th>
                  <th>UOM</th>
                  <th>QTY</th>
                  <th>Markup</th>
                  <th>SRP</th>
                  <th>Last Date Modified</th>
                  <!-- <th style="width: 40px">Label</th> -->
                </tr>
                <?php
                  if (isset($row_branch)) {
                    foreach ($row_branch as $key => $value) {
                      echo '<tr>';
                        echo '<td>'.$value['ProductID'].'</td>';
                        echo '<td>'.$value['ProductCode'].'</td>';
                        echo '<td style="text-align: center;">'.$value['Barcode'].'</td>';
                        echo '<td>'.$value['PriceModeCode'].'</td>';
                        echo '<td>'.$value['Description'].'</td>';
                        echo '<td>'.$value['uom'].'</td>';
                        echo '<td>'.$value['qty'].'</td>';
                        echo '<td>'.$value['markup'].'</td>';
                        echo '<td>'.$value['srp'].'</td>';
                        echo '<td>'.$value['LastDateModified'].'</td>';
                      echo '</tr>';
                    }
                  }
                ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <!-- <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li> -->
              </ul>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <?php } else { ?>
    <?php } ?>
  </div>
  <!-- /.content-wrapper -->
