  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="#">San Roque Supermarket</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets'); ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets'); ?>/bootstrap/js/bootstrap.min.js"></script>
<!-- PACE -->
<script src="<?php echo base_url('assets'); ?>/plugins/pace/pace.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets'); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets'); ?>/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets'); ?>/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets'); ?>/dist/js/demo.js"></script>
<!-- page script -->
<script type="text/javascript">
	// To make Pace works on Ajax calls
	$(document).ajaxStart(function() { Pace.restart(); });
    $('.ajax').click(function(){
        $.ajax({url: '#', success: function(result){
            $('.ajax-content').html('<hr>Ajax Request Completed !');
        }});
    });

  $(document).ready(function() {
    $('#am_advance').on('change', function() {
      if ($(this).filter(':checked').length > 0) {
        $('#am_advance_options').show();
      }
      else {
        $('#am_advance_options').hide();
      }
    });

    $('#aria_advance').on('change', function() {
      if ($(this).filter(':checked').length > 0) {
        $('#aria_advance_options').show();
      }
      else {
        $('#aria_advance_options').hide();
      }
    });

    $('#aria_supervisor').on('change', function() {
      if ($(this).val() == 1) {
        $('#aria_supervisor_extras').show();
      }
      else {
        $('#aria_supervisor_extras').hide();
      }
    });

    $('#am_department_id').on('change', function(){
      if ($(this).val() == 10) {
        // $('#am_role option[value=4]').attr('selected', 'selected');
        $('#am_role option[value=3]').prop('selected', false);
        $('#am_role option[value=4]').prop('selected', true);
      }
      else {
        $('#am_role option[value=4]').prop('selected', false);
        $('#am_role option[value=3]').prop('selected', true);
      }
    });

    $("#checkAllBranch").click(function () {
        $(".checkboxBranch").prop('checked', $(this).prop('checked'));
    });
    
    $(".checkboxBranch").change(function(){
        if (!$(this).prop("checked")){
            $("#checkAllBranch").prop("checked",false);
        }
    });
  });  
</script>
</body>
</html>
