<!-- Custom CSS Style for Form Error -->
<style type="text/css">
  .form-error {
    /*font-style: italic;*/
    /*font-weight: bold;*/
    color: #dd4b39;
    /*text-align: center;*/
  }
  .tooltip-inner {
    min-width: 200px;
    max-width: 350px;
    /* If max-width does not work, try using width instead */
    /*width: 350px; */
  }
</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <?php
          if (isset($mode)) {
            echo '<small><span class="label '.$mode[1].'">'.$mode[0].'</span></small>';
          } 
        ?>
      </h1>
    </section>
    <?php
      if (isset($info)) {
        echo '
            <div class="col-md-12" style="margin-top: 20px;">
              <div class="callout callout-info">
                <p><i class="icon fa fa-info-circle"></i> &nbsp; <b>Information</b> &nbsp;</br>"'.$info.'"</p>
              </div>
            </div>';
      }
    ?>
    <?php
      if (isset($aria_warning)) {
        echo '
            <div class="col-md-12">
              <div class="callout callout-danger">
                <p><i class="icon fa fa-meh-o"></i> &nbsp; <b>Warning!</b> &nbsp;</br>"'.$aria_warning.'"</p>
              </div>
            </div>';
      }
    ?>
    <section class="content" style="min-height: 150px;">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">

            <div class="box-body">
              <form action="<?php echo base_url('users/index'); ?>" method="POST">
                  <div class="col-md-offset-3 col-md-4">
                    <div class="form-group">
                      <label class="control-label">Employee ID (Biometrics) </label>
                      <input type="text" name="emp_id" class="form-control" value="<?php echo (isset($emp_id) ? $emp_id : '') ?>">
                    </div>
                    <div class="form-error">
                      <?php echo form_error('emp_id'); ?>
                    </div>
                  </div>
                  
                  <div class="col-md-2">
                    <div class="form-group">
                     <label class="control-label"></label>
                      <input type="submit" class="btn <?php echo (isset($btn_color) ? $btn_color : 'btn-danger'); ?> form-control" value="Search" style="margin-top: 5px;">
                    </div>  
                  </div>
              </form>
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      <?php 
        if (isset($res) && is_array($res)) {
          $res = $res[0];
      ?>
      <form action="<?php echo base_url('users/'.$action.''); ?>" method="POST">
        <div class="row">
          <?php echo (isset($hidden_id) ? '<input type="hidden" name="hidden_id" value="'.$hidden_id.'">' : ''); ?>
          <div class="col-md-3">
            <?php
              if (isset($aria)) {
                if (is_array($aria) && !empty($aria)) {
                  $aria = $aria[0];
                  /*echo '<div class="callout callout-success">
                          <p><i class="icon fa fa-check-circle"></i> &nbsp; Found! This will be updated!</p>
                        </div>';*/
                }
                /*else {
                  echo '<div class="callout callout-warning">
                          <p><i class="icon fa fa-warning"></i> &nbsp; Not found! This will be added!</p>
                        </div>';
                }*/
              }
            ?>
            <div class="box <?php echo (isset($box) ? $box : 'box-danger'); ?>">
              <div class="box-header">
                <h3 class="box-title">ARIA - Novaliches</h3>
                <?php 
                  if (isset($aria)) { 
                    if (is_array($aria) && !empty($aria)) {
                ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-check-circle pull-right" data-toggle="tooltip" title="Found! This will be updated!" data-placement="right" style="color: #00a65a !important; margin: 5px 0;"></i>
                </div>
                <?php } else { ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-info-circle pull-right" data-toggle="tooltip" title="Not Found! This will be Added!" data-placement="right" style="color: #00c0ef !important; margin: 5px 0;"></i>
                </div>
                <?php
                    } 
                  }
                ?>
              </div>
              <div class="box-body">
                
                <div class="form-group">
                  <label>User Login</label>
                  <input type="text" name="user_id" class="form-control" readonly="readonly" value="<?php echo (isset($res['employee_id']) ? $res['employee_id'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" class="form-control" readonly="readonly" value="<?php echo (isset($res['user_password']) ? $res['user_password'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Full Name</label>
                  <input type="text" name="real_name" class="form-control" readonly="readonly" value="<?php echo (isset($res['first_name']) ? $res['first_name'].' '.$res['last_name'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Telephone No.</label>
                  <input type="text" name="phone" class="form-control" value="<?php echo (isset($aria['phone']) ? $aria['phone'] : ''); ?>">
                </div>

                <div class="form-group">
                  <label>Email Address</label>
                  <input type="text" name="email" class="form-control" value="<?php echo (isset($aria['email']) ? $aria['email'] : ''); ?>">
                </div>

                <div class="form-group">
                  <label>Role</label>
                  <select name="Access" class="form-control">
                    <?php 
                      if (isset($aria_roles)) {
                        $selected = '';
                        foreach ($aria_roles as $key => $value) {
                          if (isset($aria['role'])) {
                            if ($aria['role'] == $value['role_id']) {
                              $selected = 'selected="selected"';
                            }
                            else {
                              $selected = '';
                            }
                          }
                          echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['role'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Language</label>
                  <select name="language" class="form-control">
                    <option value="en_GB">English</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>User's POS</label>
                  <select name="pos" class="form-control">
                    <option></option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Printing profile</label>
                  <!-- Change option value to dyanmic when 0_print_profile has value -->
                  <select name="profile" class="form-control">
                    <option selected="" value="">Browser printing support</option>
                  </select>
                </div>

                <div class="form-group">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="rep_popup" value="1" <?php echo (isset($aria['rep_popup']) && $aria['rep_popup'] == 1 ? 'checked' : 'checked'); ?>>
                      Use popup window for reports
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="can_approve_cv" value="1" <?php echo (isset($aria['can_approve_cv']) && $aria['can_approve_cv'] == 1 ? 'checked' : ''); ?>>
                      CV Approver
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="can_approve_sales_remittance" value="1" <?php echo (isset($aria['can_approve_sales_remittance']) && $aria['can_approve_sales_remittance'] == 1 ? 'checked' : ''); ?>>
                      Sales Remittance Approver
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="can_approve_sdma_1" value="1" <?php echo (isset($aria['can_approve_sdma_1']) && $aria['can_approve_sdma_1'] == 1 ? 'checked' : ''); ?>>
                      Debit Memo Approver 1
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="can_approve_sdma_2" value="1" <?php echo (isset($aria['can_approve_sdma_2']) && $aria['can_approve_sdma_2'] == 1 ? 'checked' : ''); ?>>
                      Debit Memo Approver 2
                    </label>
                  </div>

                </div>

                <div class="form-group">
                  <label>Supervisor</label>
                  <select name="is_supervisor" id="aria_supervisor" class="form-control">
                    <option value="0" <?php echo (isset($aria['is_supervisor']) && $aria['is_supervisor'] == 0 ? 'selected="selected"' : ''); ?>>No</option>
                    <option value="1" <?php echo (isset($aria['is_supervisor']) && $aria['is_supervisor'] == 1 ? 'selected="selected"' : ''); ?>>Yes</option>
                  </select>
                </div>

                <div class="form-group" id="aria_supervisor_extras" <?php echo (isset($aria['is_supervisor']) && $aria['is_supervisor'] == 1 ? '' : 'style="display: none;"'); ?>>
                  <?php 
                    $type = array(
                          1 => array('Credit Limit', 'can_credit_limit'),
                          2 => array('Negative Inventory', 'can_negative_inv'),
                          3 => array('SO Editing', 'can_details'),
                          4 => array('Voiding', 'can_void'),
                          5 => array('PO Editing', 'can_edit'),
                          6 => array('Sales Order Approval', 'can_approve_so'),
                          7 => array('Purchase Order Approval', 'can_approve_po')
                        );
                  ?>
                  <?php
                    if (isset($type)) {
                      foreach ($type as $key => $value) {
                          $checked = '';
                          if (isset($aria)) {
                            if ($aria[$value[1]] == 1) {
                              $checked = 'checked';
                            }
                            else {
                              $checked = '';
                            }
                          }
                          echo '<div class="checkbox">
                                  <label>
                                    <input type="checkbox" name="'.$value[1].'" value="1" '.$checked.'>
                                    '.$value[0].'
                                  </label>
                                </div>';
                        
                      }
                    }
                  ?>

                </div>

                <div class="form-group">
                  <label>Advance Options</label>
                  <input type="checkbox" name="aria_advance" id="aria_advance" value="1" class="pull-right" <?php echo (isset($aria_all_branch) ? 'checked' : ''); ?>>
                </div>

                <div class="form-group" id="aria_advance_options" <?php echo (isset($aria_all_branch) ? '' : 'style="display: none;"'); ?>>
                  <label>Other Branches</label>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" id="checkAllBranch">
                      Check All
                    </label>
                  </div>
                  <?php
                    if (isset($aria_branches)) {
                      $checked = '';
                      foreach ($aria_branches as $key => $value) {
                          if (isset($aria_all_branch)) {
                            if ($aria_all_branch[$key][$value[1]] === 'true') {
                              $checked = 'checked disabled';
                            }
                            else {
                              $checked = '';
                            }
                          }
                          echo '<div class="checkbox">
                                  <label>
                                    <input type="checkbox" name="branches[]" class="'.(!empty($checked) ? '' : 'checkboxBranch').'" value="'.$value[1].'" '.$checked.'>
                                    '.$value[0].'
                                  </label>';
                          if (!empty($checked)) {
                            echo '<input type="hidden" name="branches[]" value="'.$value[1].'">';
                          }
                          if (isset($aria_all_branch)) {
                            if ($aria_all_branch[$key][$value[1]] === 'true') {
                              echo '<i class="icon fa fa-check-circle pull-right" data-toggle="tooltip" title="Found! This will be updated!" data-placement="right" style="color: #00a65a !important; margin: 5px 0;"></i>';
                            }
                            else {
                              echo '<i class="icon fa fa-info-circle pull-right" data-toggle="tooltip" title="Not Found! tick checkbox if you want to add this to this branch!" data-placement="right" style="color: #00c0ef !important; margin: 5px 0;"></i>';
                            }
                          }
                          echo '</div>'; 
                          
                      }
                    }
                  ?>
                </div>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->


          </div>

          <div class="col-md-3">
            <?php
              if (isset($am)) {
                if (is_array($am) && !empty($am)) {
                  $am = $am[0];
                  /*echo '<div class="callout callout-success">
                          <p><i class="icon fa fa-check-circle"></i> &nbsp; Found! This will be updated!</p>
                        </div>';*/
                }
                /*else {
                  echo '<div class="callout callout-warning">
                          <p><i class="icon fa fa-warning"></i> &nbsp; Not found! This will be added!</p>
                        </div>';
                }*/
              }
            ?>
            <div class="box <?php echo (isset($box) ? $box : 'box-danger'); ?>">
              <div class="box-header">
                <h3 class="box-title">Asset Management</h3>
                <?php 
                  if (isset($am)) { 
                    if (is_array($am) && !empty($am)) {
                ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-check-circle pull-right" data-toggle="tooltip" title="Found! This will be updated!" data-placement="right" style="color: #00a65a !important; margin: 5px 0;"></i>
                </div>
                <?php } else { ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-info-circle pull-right" data-toggle="tooltip" title="Not Found! This will be Added!" data-placement="right" style="color: #00c0ef !important; margin: 5px 0;"></i>
                </div>
                <?php
                    } 
                  }
                ?>
              </div>
              <div class="box-body">
                
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="am_username" class="form-control" readonly="readonly" value="<?php echo (isset($res['employee_id']) ? $res['employee_id'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="am_password" class="form-control" readonly="readonly" value="<?php echo (isset($res['user_password']) ? $res['user_password'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Employee No.</label>
                  <input type="text" name="am_emp_number" class="form-control" readonly="readonly" value="<?php echo (isset($res['emp_number']) ? $res['emp_number'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" name="am_fname" class="form-control" readonly="readonly" value="<?php echo (isset($res['first_name']) ? $res['first_name'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" name="am_lname" class="form-control" readonly="readonly" value="<?php echo (isset($res['last_name']) ? $res['last_name'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Department</label>
                  <select name="am_department_id" id="am_department_id" class="form-control">
                    <?php 
                      if (isset($orange_dep)) {
                        $selected = '';
                        foreach ($orange_dep as $key => $value) {
                          if (isset($am['department_id']) && $am['department_id'] == $value['department_id']) {
                            $selected = 'selected="selected"';
                          }
                          elseif (isset($res['department_id']) && $res['department_id'] == $value['department_id'] && !isset($am['department_id'])) {
                            $selected = 'selected="selected"';
                          }
                          else {
                            $selected = '';
                          }
                          
                          echo '<option value="'.$value['department_id'].'" '.$selected.'>'.$value['name'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Role</label>
                  <select name="am_role" id="am_role" class="form-control">
                    <?php 
                      if (isset($am_roles)) {
                        $selected = '';
                        foreach ($am_roles as $key => $value) {
                          if (isset($am['role']) && $am['role'] == $value['id']) {
                            $selected = 'selected="selected"';
                          }
                          elseif ($res['department_id'] == 10 && $value['id'] == 4 && !isset($am['role'])) {
                            $selected = 'selected="selected"';
                          }
                          elseif ($value['id'] == 3 && !isset($am['role'])) {
                            $selected = 'selected="selected"';
                          }
                          else {
                            $selected = '';
                          }
                          
                          echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['role'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Advance Options</label>
                  <input type="checkbox" name="am_advance" id="am_advance" value="1" class="pull-right" <?php echo (isset($am['sub_department_id']) && !empty($am['sub_department_id']) ? 'checked' : '' ); ?>>
                </div>

                <div class="form-group" id="am_advance_options" <?php echo (isset($am['sub_department_id']) && !empty($am['sub_department_id']) ? '' : 'style="display: none;"' ); ?>>
                  <label>Sub Department</label>
                  <select name="am_sub_department_id" class="form-control">
                    <?php 
                      if (isset($orange_sub_dep)) {
                        $selected = '';
                        foreach ($orange_sub_dep as $key => $value) {
                          if (isset($am['sub_department_id']) && !empty($am['sub_department_id'])) {
                            if ($am['sub_department_id'] == $value['id']) {
                              $selected = 'selected="selected"';
                            }
                            else {
                              $selected = '';
                            }
                          }
                          echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['name'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->


          </div>

          <div class="col-md-3">
            <?php
              if (isset($po)) {
                if (is_array($po) && !empty($po)) {
                  $po = $po[0];
                  /*echo '<div class="callout callout-success">
                          <p><i class="icon fa fa-check-circle"></i> &nbsp; Found! This will be updated!</p>
                        </div>';*/
                }
                /*else {
                  echo '<div class="callout callout-warning">
                          <p><i class="icon fa fa-warning"></i> &nbsp; Not found! This will be added!</p>
                        </div>';
                }*/
              }
            ?>
            <div class="box <?php echo (isset($box) ? $box : 'box-danger'); ?>">
              <div class="box-header">
                <h3 class="box-title">Purchase Order</h3>
                <?php 
                  if (isset($po)) { 
                    if (is_array($po) && !empty($po)) {
                ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-check-circle pull-right" data-toggle="tooltip" title="Found! This will be updated!" data-placement="right" style="color: #00a65a !important; margin: 5px 0;"></i>
                </div>
                <?php } else { ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-info-circle pull-right" data-toggle="tooltip" title="Not Found! This will be Added!" data-placement="right" style="color: #00c0ef !important; margin: 5px 0;"></i>
                </div>
                <?php
                    } 
                  }
                ?>
              </div>
              <div class="box-body">
                
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="po_username" class="form-control" readonly="readonly" value="<?php echo (isset($res['employee_id']) ? $res['employee_id'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="po_password" class="form-control" readonly="readonly" value="<?php echo (isset($res['user_password']) ? $res['user_password'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Employee No.</label>
                  <input type="text" name="po_emp_code" class="form-control" readonly="readonly" value="<?php echo (isset($res['emp_number']) ? $res['emp_number'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" name="po_fname" class="form-control" readonly="readonly" value="<?php echo (isset($res['first_name']) ? $res['first_name'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" name="po_lname" class="form-control" readonly="readonly" value="<?php echo (isset($res['last_name']) ? $res['last_name'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Gender</label>
                  <select name="po_gender" class="form-control">
                    <option value="male" <?php echo (isset($po['gender']) && $po['gender'] == 'male' ? 'selected="selected"' : ''); ?>>Male</option>
                    <option value="female" <?php echo (isset($po['gender']) && $po['gender'] == 'female' ? 'selected="selected"' : ''); ?>>Female</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Role</label>
                  <select name="po_role" class="form-control">
                    <?php 
                      if (isset($po_roles)) {
                        $selected = '';
                        foreach ($po_roles as $key => $value) {
                          if (isset($po['role'])) {
                            if ($po['role'] == $value['id']) {
                              $selected = 'selected="selected"';
                            }
                            else {
                              $selected = '';
                            }
                          }
                          echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['role'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->


          </div>

          <div class="col-md-3">
            <?php
              if (isset($cm)) {
                if (is_array($cm) && !empty($cm)) {
                  $cm = $cm[0];
                  /*echo '<div class="callout callout-success">
                          <p><i class="icon fa fa-check-circle"></i> &nbsp; Found! This will be updated!</p>
                        </div>';*/
                }
                /*else {
                  echo '<div class="callout callout-warning">
                          <p><i class="icon fa fa-warning"></i> &nbsp; Not found! This will be added!</p>
                        </div>';
                }*/
              }
            ?>
            <div class="box <?php echo (isset($box) ? $box : 'box-danger'); ?>">
              <div class="box-header">
                <h3 class="box-title">Customer Maintenance</h3>
                <?php 
                  if (isset($cm)) { 
                    if (is_array($cm) && !empty($cm)) {
                ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-check-circle pull-right" data-toggle="tooltip" title="Found! This will be updated!" data-placement="left" style="color: #00a65a !important; margin: 5px 0;"></i>
                </div>
                <?php } else { ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-info-circle pull-right" data-toggle="tooltip" title="Not Found! This will be Added!" data-placement="left" style="color: #00c0ef !important; margin: 5px 0;"></i>
                </div>
                <?php
                    } 
                  }
                ?>
              </div>
              <div class="box-body">
                
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="cm_username" class="form-control" readonly="readonly" value="<?php echo (isset($res['employee_id']) ? $res['employee_id'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="cm_password" class="form-control" readonly="readonly" value="<?php echo (isset($res['user_password']) ? $res['user_password'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" name="cm_fname" class="form-control" readonly="readonly" value="<?php echo (isset($res['first_name']) ? $res['first_name'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" name="cm_lname" class="form-control" readonly="readonly" value="<?php echo (isset($res['last_name']) ? $res['last_name'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Branch</label>
                  <select name="cm_branch" class="form-control">
                    <option value="all">All</option>
                    <?php 
                      if (isset($cm_branches)) {
                        $selected = '';
                        foreach ($cm_branches as $key => $value) {
                          if (isset($cm['branch']) && !empty($cm['branch'])) {
                            if ($cm['branch'] == $value['code']) {
                              $selected = 'selected="selected"';
                            }
                            else {
                              $selected = '';
                            }
                          }
                          echo '<option value="'.$value['code'].'" '.$selected.'>'.str_replace('San Roque Supermarket Retail Systems Inc. - ', '', $value['name']).'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Level</label>
                  <select name="cm_level" class="form-control">
                     <option value="0">None</option>
                     <option value="1">Supervisor (for lost card)</option>
                     <option value="2">Administrator (manager)</option>
                  </select>
                </div>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <?php
              if (isset($sr)) {
                if (is_array($sr) && !empty($sr)) {
                  $sr = $sr[0];
                  /*echo '<div class="callout callout-success">
                          <p><i class="icon fa fa-check-circle"></i> &nbsp; Found! This will be updated!</p>
                        </div>';*/
                }
                /*else {
                  echo '<div class="callout callout-warning">
                          <p><i class="icon fa fa-warning"></i> &nbsp; Not found! This will be added!</p>
                        </div>';
                }*/
              }
            ?>

            <div class="box <?php echo (isset($box) ? $box : 'box-danger'); ?>">
              <div class="box-header">
                <h3 class="box-title">Sales Report</h3>
                <?php 
                  if (isset($sr)) { 
                    if (is_array($sr) && !empty($sr)) {
                ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-check-circle pull-right" data-toggle="tooltip" title="Found! This will be updated!" data-placement="left" style="color: #00a65a !important; margin: 5px 0;"></i>
                </div>
                <?php } else { ?>
                <div class="pull-right" style="font-size: 1.2em;">
                  <i class="icon fa fa-info-circle pull-right" data-toggle="tooltip" title="Not Found! This will be Added!" data-placement="left" style="color: #00c0ef !important; margin: 5px 0;"></i>
                </div>
                <?php
                    } 
                  }
                ?>
              </div>
              <div class="box-body">
                
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="sr_username" class="form-control" readonly="readonly" value="<?php echo (isset($res['employee_id']) ? $res['employee_id'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="sr_password" class="form-control" readonly="readonly" value="<?php echo (isset($res['user_password']) ? $res['user_password'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" name="sr_fname" class="form-control" readonly="readonly" value="<?php echo (isset($res['first_name']) ? $res['first_name'] : ''); ?>" >
                </div>

                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" name="sr_lname" class="form-control" readonly="readonly" value="<?php echo (isset($res['last_name']) ? $res['last_name'] : ''); ?>" >
                </div>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->


          </div>


          <div class="col-md-12">
            
            <div class="box">
              <!-- <div class="box-header">
                <h3 class="box-title">Asset Management</h3>
              </div> -->
              <div class="box-body">
                <div class="form-group">
                  <input type="submit" name="submit" value="<?php echo (isset($add_update) ? $add_update : 'Add User'); ?>" class="btn <?php echo (isset($btn_color) ? $btn_color : 'btn-danger'); ?> pull-right" style="width: 10%;">
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->


          </div>
        </div>
      </form>
      <?php } ?>

    </section>
    
  </div>
  <!-- /.content-wrapper -->
