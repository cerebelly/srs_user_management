<?php 
defined('BASEPATH') OR exit('No direct script access allowed!');

class Monitoring_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_duplicates($branch, $barcode) {
		load_db($branch);
		$sql = 'SELECT *
				FROM POS_Products
				WHERE Barcode IN (
				    SELECT Barcode
				    FROM POS_Products
				    GROUP BY Barcode
				    HAVING COUNT(ProductID) > 1
				    )';
				    
		if (!empty($barcode)) {
			$sql .= ' AND Barcode = "'.$barcode.'"';
		}

		$sql .= ' ORDER BY Barcode';
				
		$query = $this->db->query($sql);
		return $query->result_array();
	}

}

?>