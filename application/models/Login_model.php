<?php 
defined('BASEPATH') OR exit('No direct script access allowed!');;

class Login_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function user_exist($data) {
		$cond = "`user_name` = '".$data['username']."' AND `user_password` = '".$data['password']."'";
		$this->db->select('*');
		$this->db->from('hs_hr_users');
		$this->db->where($cond);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return true;
		} 
		else {
			return false;
		}
	}

	public function user_fetch($data) {
		$cond = "`user_name` = '".$data['username']."'";
		$this->db->select('emp_number');
		$this->db->from('hs_hr_users');
		$this->db->where($cond);
		$this->db->limit(1);
		$query = $this->db->get();

		$result = $query->result();

		if($query->num_rows() > 0) {
			$emp_id = $result[0]->emp_number;
			$cond = "c.emp_id = ".$emp_id." AND b.department_id = c.department_id AND a.emp_number = ".$emp_id."";
			$this->db->select('a.*, c.emp_id, b.department_id, b.`name`');
			$this->db->from('hs_hr_users a,  hr_department b, hr_emp_job_info c');
			$this->db->where($cond);
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->result_array();
			if($query->num_rows() > 0) {
				return $result[0];
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}	

	public function add_user_info($user_info, $sess_info) {
		if (!empty($user_info) && !empty($sess_info)) {
			$data = array(
				'session_id' => $sess_info['session_id'],
				'ip_address' => $sess_info['ip_address'],
				'user_agent' => $sess_info['user_agent'],
				'time_stamp' => $sess_info['time_stamp'],
				'logged_date' => $sess_info['date_time'],
				'logged_info' => serialize($user_info)
			);
			load_db('logs');
			$query = $this->db->insert('user_sessions', $data);
		}
	}	


}


?>