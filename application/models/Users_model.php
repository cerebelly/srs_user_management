<?php 
defined('BASEPATH') OR exit('No direct script access allowed!');

class Users_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_employee($emp_id) {
		load_db('default');
		$sql = 'SELECT *
				FROM hs_hr_users a, hr_emp_job_info b, hs_hr_employee c
				WHERE c.employee_id = '.$emp_id.'
				AND a.emp_number = c.emp_number
				AND b.emp_id = c.emp_number';
		$query = $this->db->query($sql);
		$result = $query->result_array();
		if ($query->num_rows() > 0) {
			return $result;
		}
		else {
			return false;
		}
	}

	public function get_aria_roles() {
		load_db('my_novaliches');
		$sql = 'SELECT id, role, inactive FROM 0_security_roles ORDER BY role';
		$query = $this->db->query($sql);
		$result = $query->result_array();
		if ($query->num_rows() > 0) {
			return $result;
		}
		else {
			return false;
		}
	}

	public function get_orange_dep() {
		load_db('default');
		$this->db->order_by('department_id');
		$query = $this->db->get('hr_department');
		return $query->result_array();
	}

	public function get_am_roles() {
		load_db('asset_mngt');
		$this->db->order_by('id');
		$query = $this->db->get('user_roles');
		return $query->result_array();
	}

	public function get_orange_sub_dep() {
		load_db('default');
		// $this->db->order_by('name');
		$query = $this->db->get('hr_sub_department');
		return $query->result_array();
	}

	public function get_cm_branches() {
		load_db('cust_main');
		$query = $this->db->get('branches');
		return $query->result_array();
	}

	public function get_po_roles() {
		load_db('po');
		$query = $this->db->get('user_roles');
		return $query->result_array();
	}

	/*public function check_if_user_exist($emp_id) {
		$result = array();

		load_db('central_aria');
		$this->db->where('user_id', $emp_id);
		$query_aria = $this->db->get('0_users');
		if ($query_aria->num_rows() > 0) {
			array_push($result, $query_aria->result_array());
		}
		else {
			array_push($result, '');
		}

		load_db('asset_mngt');
		$this->db->where('username', $emp_id);
		$query_am = $this->db->get('users');
		if ($query_am->num_rows() > 0) {
			array_push($result, $query_am->result_array());
		}
		else {
			array_push($result, '');
		}

		load_db('po');
		$this->db->where('user', $emp_id);
		$query_po = $this->db->get('users');
		if ($query_po->num_rows() > 0) {
			array_push($result, $query_po->result_array());
		}
		else {
			array_push($result, '');
		}

		load_db('cust_main');
		$this->db->where('username', $emp_id);
		$query_cm = $this->db->get('users');
		if ($query_cm->num_rows() > 0) {
			array_push($result, $query_cm->result_array());
		}
		else {
			array_push($result, '');
		}

		load_db('sales_report');
		$this->db->where('username', $emp_id);
		$query_sr = $this->db->get('users');
		if ($query_sr->num_rows() > 0) {
			array_push($result, $query_sr->result_array());
		}
		else {
			array_push($result, '');
		}

		if (is_array($result) && !empty($result)) {
			return $result;
		}
		else {
			return false;
		}

	}*/

	public function check_if_user_exist_aria($emp_id) {
		load_db('my_novaliches');
		$this->db->where('user_id', $emp_id);
		$query_aria = $this->db->get('0_users');
		return $query_aria->result_array();
	}

	public function check_if_user_exist_aria_nova($emp_id) {
		load_db('my_novaliches'); 
		$this->db->where('user_id', $emp_id);
		$query_aria = $this->db->get('0_users');
		return $query_aria->result_array();
	}

	public function check_if_user_exist_aria_branches($emp_id, $branches) {
		$container = array();
		$checker = array();
		if (!empty($emp_id) && !empty($branches)) {
			foreach ($branches as $key => $value) {
				load_db($value[1]);
				$this->db->where('user_id', $emp_id);
				$query = $this->db->get('0_users');
				if ($query->num_rows() > 0) {
					array_push($container, array($value[1] => 'true'));
					array_push($checker, 'true');
				}
				else {
					array_push($container, array($value[1] => 'false'));
					array_push($checker, 'false');
				}
			}
		}
		$bool = false;
		if (count(array_unique($checker)) !== 1) {
			$bool = true;
		}
		return array($container, $bool);
	}

	public function check_if_user_exist_aria_branch($emp_id, $branch) {
		load_db($branch); 
		$this->db->where('user_id', $emp_id);
		$query = $this->db->get('0_users');
		return $query->result_array();
	}

	public function check_if_user_exist_am($emp_id) {
		load_db('asset_mngt');
		$this->db->where('username', $emp_id);
		$query_am = $this->db->get('users');
		return $query_am->result_array();
	}

	public function check_if_user_exist_po($emp_id) {
		load_db('po');
		$this->db->where('user', $emp_id);
		$query_po = $this->db->get('users');
		return $query_po->result_array();
	}

	public function check_if_user_exist_cm($emp_id) {
		load_db('cust_main');
		$this->db->where('username', $emp_id);
		$query_cm = $this->db->get('users');
		return $query_cm->result_array();
	}

	public function check_if_user_exist_sr($emp_id) {
		load_db('sales_report');
		$this->db->where('username', $emp_id);
		$query_sr = $this->db->get('users');
		return $query_sr->result_array();
	}

	public function insert_aria_user($data) {
		load_db('my_novaliches');
		$query = $this->db->insert('0_users', $data);
		if ($this->db->affected_rows() > 0) {
			logger(__FUNCTION__, $this->db->last_query());
		}
	}

	public function insert_aria_nova_user($data) {
		load_db('my_novaliches');
		$query = $this->db->insert('0_users', $data);
		if ($this->db->affected_rows() > 0) {
			logger(__FUNCTION__, $this->db->last_query());
		}
	}

	public function insert_am_user($data) {
		load_db('asset_mngt');
		$query = $this->db->insert('users', $data);
		if ($this->db->affected_rows() > 0) {
			logger(__FUNCTION__, $this->db->last_query());
		}
	}

	public function insert_sr_user($data) {
		load_db('sales_report');
		$query = $this->db->insert('users', $data);
		if ($this->db->affected_rows() > 0) {
			logger(__FUNCTION__, $this->db->last_query());
		}
	}

	public function insert_cm_user($data) {
		load_db('cust_main');
		$query = $this->db->insert('users', $data);
		if ($this->db->affected_rows() > 0) {
			logger(__FUNCTION__, $this->db->last_query());
		}
	}

	public function insert_po_user($data) {
		load_db('po');
		$query = $this->db->insert('users', $data);
		if ($this->db->affected_rows() > 0) {
			logger(__FUNCTION__, $this->db->last_query());
		}
	}

	public function insert_aria_branch_user($data, $branch) {
		load_db($branch);
		$query = $this->db->insert('0_users', $data);
		if ($this->db->affected_rows() > 0) {
			logger(__FUNCTION__, $this->db->last_query());
		}
	}

	public function update_aria_user($data, $emp_id) {
		$arr = $this->check_if_user_exist_aria($emp_id);
		if (!empty($arr)) {
			load_db('my_novaliches');
			$this->db->where('user_id', $emp_id);
			$query = $this->db->update('0_users', $data);
			if ($this->db->affected_rows() > 0) {
				logger(__FUNCTION__, $this->db->last_query(), $arr);
			}
		}
		else {
			$this->insert_aria_user($data);
		}
	}

	public function update_aria_nova_user($data, $emp_id) {
		$arr = $this->check_if_user_exist_aria_nova($emp_id);
		if (!empty($arr)) {
			load_db('my_novaliches');
			$this->db->where('user_id', $emp_id);
			$query = $this->db->update('0_users', $data);
			if ($this->db->affected_rows() > 0) {
				logger(__FUNCTION__, $this->db->last_query(), $arr);
			}
		}
		else {
			$this->insert_aria_nova_user($data);
		}
	}

	public function update_aria_branch_user($data, $emp_id, $branches) {
		if (!empty($branches) && !empty($emp_id)) {
			foreach ($branches as $key => $value) {
				$arr = $this->check_if_user_exist_aria_branch($emp_id, $value);
				if (!empty($arr)) {
					load_db($value);
					$this->db->where('user_id', $emp_id);
					$query = $this->db->update('0_users', $data);
					if ($this->db->affected_rows() > 0) {
						logger(__FUNCTION__, $this->db->last_query(), $arr);
					}
				}
				else {
					$this->insert_aria_branch_user($data, $value);
				}
			}
		}
		else {
			if (!empty($branches)) {
				foreach ($branches as $key => $value) {
					$this->insert_aria_branch_user($data, $value);
				}
			}
		}
		
	}


	public function update_am_user($data, $emp_id) {
		$arr = $this->check_if_user_exist_am($emp_id);
		if (!empty($arr)) {
			load_db('asset_mngt');
			$this->db->where('username', $emp_id);
			$query = $this->db->update('users', $data);
			if ($this->db->affected_rows() > 0) {
				logger(__FUNCTION__, $this->db->last_query(), $arr);
			}
		}
		else {
			$this->insert_am_user($data);
		}
	}

	public function update_sr_user($data, $emp_id) {
		$arr = $this->check_if_user_exist_sr($emp_id);
		if (!empty($arr)) {
			load_db('sales_report');
			$this->db->where('username', $emp_id);
			$query = $this->db->update('users', $data);
			if ($this->db->affected_rows() > 0) {
				logger(__FUNCTION__, $this->db->last_query(), $arr);
			}
		}
		else {
			$this->insert_sr_user($data);
		}
	}

	public function update_cm_user($data, $emp_id) {
		$arr = $this->check_if_user_exist_cm($emp_id);
		if (!empty($arr)) {
			load_db('cust_main');
			$this->db->where('username', $emp_id);
			$query = $this->db->update('users', $data);
			if ($this->db->affected_rows() > 0) {
				logger(__FUNCTION__, $this->db->last_query(), $arr);
			}
		}
		else {
			$this->insert_cm_user($data);
		}
	}

	public function update_po_user($data, $emp_id) {
		$arr = $this->check_if_user_exist_po($emp_id);
		if (!empty($arr)) {
			load_db('po');
			$this->db->where('user', $emp_id);
			$query = $this->db->update('users', $data);
			if ($this->db->affected_rows() > 0) {
				logger(__FUNCTION__, $this->db->last_query(), $arr);
			}
		}
		else {
			$this->insert_po_user($data);
		}
	}

	public function get_sr() {
		load_db('sales_report');
		$this->db->limit(10);
		$query = $this->db->get('users');
		return $query->result_array();
	}

	public function get_am() {
		load_db('asset_mngt');
		$this->db->limit(10);
		$query = $this->db->get('users');
		return $query->result_array();
	}

	public function get_log($log_id) {
		load_db('logs');
		$sql = "SELECT * FROM user_logs WHERE log_id = $log_id";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			return $result[0];
		} else {
			return false;
		}
		
	}


}


?>