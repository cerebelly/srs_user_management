<?php 
defined('BASEPATH') OR exit('No direct script access allowed!');

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library(array('form_validation', 'session'));
		$this->load->model('Users_model');
	}	

	public function index() {
		if (isset($this->session->userdata['logged_in'])) {
			$this->body = '';
			$this->data['mode'] = array("Search Mode", "label-danger");
			$this->form_validation->set_rules('emp_id', 'Employee ID', 'trim|required|numeric|xss_clean');

			$aria_branches = array(
					// array('Novaliches', 'my_novaliches'),
					array('Malabon', 'my_malabon'),
					array('Kusina Malabon', 'my_kusina_malabon'),
					array('Navotas', 'my_navotas'),
					array('Pavia', 'my_pavia'),
					array('Gagalangin', 'my_gagalangin'),
					array('Pateros', 'my_pateros'),
					array('Comembo', 'my_comembo'),
					array('Bagong Silang', 'my_bagong_silang'),
					array('Camarin', 'my_camarin'),
					array('Las Pinas', 'my_las_pinas'),
					array('Imus', 'my_imus'),
					array('Antipolo 1', 'my_antipolo_1'),
					array('Antipolo 2', 'my_antipolo_2'),
					array('Cainta 1', 'my_cainta_1'),
					array('Cainta 2', 'my_cainta_2'),
					array('Gen. T. De Leon', 'my_gt_deleon'),
					array('Punturin', 'my_punturin'),
					array('San Pedro', 'my_san_pedro'),
					array('Alaminos', 'my_alaminos')
				);
			$this->data['aria_branches'] = $aria_branches;

			$aria_roles = $this->Users_model->get_aria_roles();
			$this->data['aria_roles'] = $aria_roles;

			$orange_dep = $this->Users_model->get_orange_dep();
			$this->data['orange_dep'] = $orange_dep;

			$am_roles = $this->Users_model->get_am_roles();
			$this->data['am_roles'] = $am_roles;

			$orange_sub_dep = $this->Users_model->get_orange_sub_dep();
			$this->data['orange_sub_dep'] = $orange_sub_dep;

			$cm_branches = $this->Users_model->get_cm_branches();
			$this->data['cm_branches'] = $cm_branches;

			$po_roles = $this->Users_model->get_po_roles();
			$this->data['po_roles'] = $po_roles;

			if (sizeof($_POST) > 0) {
				if($this->form_validation->run() != false) {
					// pre_r($_POST);
					$emp_id = $this->input->post('emp_id');

					// $aria = $this->Users_model->check_if_user_exist_aria($emp_id);
					$aria = $this->Users_model->check_if_user_exist_aria_nova($emp_id);
					$aria_all_branch = $this->Users_model->check_if_user_exist_aria_branches($emp_id, $aria_branches);
					// pre_r($aria_all_branch); exit;
					$am = $this->Users_model->check_if_user_exist_am($emp_id);
					$po = $this->Users_model->check_if_user_exist_po($emp_id);
					$cm = $this->Users_model->check_if_user_exist_cm($emp_id);
					$sr = $this->Users_model->check_if_user_exist_sr($emp_id);

					if (!empty($aria) || !empty($am) || !empty($po) || !empty($cm) || !empty($sr) || $aria_all_branch[1] == true) {
						// if (array_filter($check)) {
							$this->data['action'] = "editUser";
							$this->body['skin'] = "skin-blue";
							$this->body['btn_color'] = "btn-primary";
							$this->body['add_update'] = "Update User";
							$this->body['box'] = "box-primary";
							$this->data['mode'] = array("Edit User Mode", "label-primary");
							// list($aria, $am, $po, $cm, $sr) = $check;
							/*pre_r($aria);
							pre_r($am);
							pre_r($po);
							pre_r($cm);
							pre_r($sr);*/
							$this->data['aria'] = $aria;
							if (!empty($aria_all_branch[0])) {
								$this->data['aria_all_branch'] = $aria_all_branch[0];
							}
							$this->data['am'] = $am;
							$this->data['po'] = $po;
							$this->data['cm'] = $cm;
							$this->data['sr'] = $sr;
							$this->data['info'] = "If in any cases your Employee ID has been found and is already existing in some of the listed system but do not match with your personal information such as your name or employee no., Please do ask the administrator to delete or apply correct action for the certain issue to avoid duplicate and unwanted information.";
							if ($aria_all_branch[1] == true && empty($aria)) {
								$this->data['aria_warning'] = "Hey! before you save and update changes please do ask your system administrator because we've notice that the employee ID you've entered have an existing account in a branch (scroll down to look for that certain branch in aria!) but not existing in the main branch (Novaliches). Note that this will overwrite any of that existing information in the given branch so think twice before you proceed!";
							}

							$res = $this->Users_model->get_employee($emp_id);
							
							if ($res != false) {
								$new_res = array();
								foreach ($res as $key => $value) {
									$res_arr = array(
										'id' => $value['id'],
										'employee_id' => $value['employee_id'],
										'user_name' => $value['user_name'],
										'user_password' => $value['user_password'],
										'first_name' => $value['first_name'],
										'last_name' => $value['last_name'],
										'emp_number' => $value['emp_number'],
										'is_admin' => $value['is_admin'],
										'created_by' => $value['created_by'],
										'userg_id' => $value['userg_id'],
										'department_id' => $value['department_id'],
										'sub_department_id' => $value['sub_department_id'],
										'branch_id' => $value['branch_id'],
										'bio_branch_id' => $value['bio_branch_id']
									);
									array_push($new_res, $res_arr);
								}
								$this->data['res'] = $new_res;
							}

							$this->data['hidden_id'] = $emp_id;
						// }
					}
					else {
						$this->data['action'] = "addUser";
						$this->data['mode'] = array("Add User Mode", "label-danger");
						$res = $this->Users_model->get_employee($emp_id);
						// pre_r($res);
						if ($res != false) {
							$new_res = array();
							foreach ($res as $key => $value) {
								$res_arr = array(
									'id' => $value['id'],
									'employee_id' => $value['employee_id'],
									'user_name' => $value['user_name'],
									'user_password' => $value['user_password'],
									'first_name' => $value['first_name'],
									'last_name' => $value['last_name'],
									'emp_number' => $value['emp_number'],
									'is_admin' => $value['is_admin'],
									'created_by' => $value['created_by'],
									'userg_id' => $value['userg_id'],
									'department_id' => $value['department_id'],
									'sub_department_id' => $value['sub_department_id'],
									'branch_id' => $value['branch_id'],
									'bio_branch_id' => $value['bio_branch_id']
								);
								array_push($new_res, $res_arr);
							}
							$this->data['res'] = $new_res;
						}
					}


					$this->data['emp_id'] = $this->input->post('emp_id');
				}
			}

			$this->load->view('common/header', $this->body);
			$this->load->view('common/sidebar');
			$this->load->view('users', $this->data);
			$this->load->view('common/footer');	
		}
		else {
			redirect('login');
		}
		
	}

	public function addUser() {
		// pre_r($_POST); exit;
		if (sizeof($_POST) > 0) {

			# ARIA
			$user_id = $this->input->post('user_id');
			$password = $this->input->post('password');
			$real_name = $this->input->post('real_name');
			$phone = $this->input->post('phone');
			$email = $this->input->post('email');
			$Access = $this->input->post('Access');
			$language = $this->input->post('language');
			$pos = $this->input->post('pos');
			$profile = $this->input->post('profile');
			$rep_popup = $this->input->post('rep_popup');
			$can_approve_cv = $this->input->post('can_approve_cv');
			$can_approve_sales_remittance = $this->input->post('can_approve_sales_remittance');
			$can_approve_sdma_1 = $this->input->post('can_approve_sdma_1');
			$can_approve_sdma_2 = $this->input->post('can_approve_sdma_2');
			$is_supervisor = $this->input->post('is_supervisor');
			$can_credit_limit = $this->input->post('can_credit_limit');
			$can_negative_inv = $this->input->post('can_negative_inv');
			$can_details = $this->input->post('can_details');
			$can_void = $this->input->post('can_void');
			$can_edit = $this->input->post('can_edit');
			$can_approve_so = $this->input->post('can_approve_so');
			$can_approve_po = $this->input->post('can_approve_po');

			$branches = $this->input->post('branches');

			if (empty($pos)) 
				$pos = 1; //should be 0

			/*if (empty($profile)) 
				$profile = ''; 

			if (empty($phone)) 
				$phone = ''; 

			if (empty($email)) 
				$email = ''; */


			if (empty($can_approve_cv)) 
				$can_approve_cv = 0;

			if (empty($can_approve_sales_remittance)) 
				$can_approve_sales_remittance = 0;

			if (empty($can_approve_sdma_1)) 
				$can_approve_sdma_1 = 0;

			if (empty($can_approve_sdma_2)) 
				$can_approve_sdma_2 = 0;

			# is_supervisor settings ...

			if (empty($can_credit_limit)) 
				$can_credit_limit = 0;

			if (empty($can_negative_inv)) 
				$can_negative_inv = 0; 

			if (empty($can_details)) 
				$can_details = 0; 

			if (empty($can_void)) 
				$can_void = 0; 

			if (empty($can_edit)) 
				$can_edit = 0; 

			if (empty($can_approve_so)) 
				$can_approve_so = 0; 

			if (empty($can_approve_po)) 
				$can_approve_po = 0; 


			// $aria_data = array();
			if ($is_supervisor == 1) {
				$aria_data = array(
						'user_id' => $user_id, 
						'password' =>  $password, 
						'real_name' =>  $real_name,
						'phone' =>  $phone,
						'email' => $email,
						'role_id' => $Access,
						'language' => $language,
						'pos' => $pos,
						'print_profile' => $profile,
						'rep_popup' => $rep_popup,
						'can_approve_cv' => $can_approve_cv,
						'can_approve_sales_remittance' => $can_approve_sales_remittance,
						'can_approve_sdma_1' => $can_approve_sdma_1,
						'can_approve_sdma_2' => $can_approve_sdma_2,
						'is_supervisor' => $is_supervisor, 
						'can_credit_limit' => $can_credit_limit,
						'can_negative_inv' => $can_negative_inv,
						'can_details' => $can_details,
						'can_void' => $can_void,
						'can_edit' => $can_edit,
						'can_approve_so' => $can_approve_so, 
						'can_approve_po' => $can_approve_po,
						'date_format' => 0,
						'date_sep' => 0,
						'tho_sep' => 0,
						'dec_sep' => 0,
						'theme' => 'modern',
						'page_size' => 'Letter', 
						'prices_dec' => 2,
						'qty_dec' => 2,
						'rates_dec' => 4,
						'percent_dec' => 1,
						'show_gl' => 1,
						'show_codes' => 0,
						'show_hints' => 0,
						'query_size' => 10,
						'graphic_links' => 1,
						'sticky_doc_date' => 0,
						'startup_tab' => 'AP',
						'inactive' => 0,
						'allow_user' => 0
					);
			}
			else {
				$aria_data = array(
						'user_id' => $user_id, 
						'password' =>  $password, 
						'real_name' =>  $real_name,
						'phone' =>  $phone,
						'email' => $email,
						'role_id' => $Access,
						'language' => $language,
						'pos' => $pos,
						'print_profile' => $profile,
						'rep_popup' => $rep_popup,
						'can_approve_cv' => $can_approve_cv,
						'can_approve_sales_remittance' => $can_approve_sales_remittance,
						'can_approve_sdma_1' => $can_approve_sdma_1,
						'can_approve_sdma_2' => $can_approve_sdma_2,
						'is_supervisor' => $is_supervisor, 
						'can_credit_limit' => 0,
						'can_negative_inv' => 0,
						'can_details' => 0,
						'can_void' => 0,
						'can_edit' => 0,
						'can_approve_so' => 0, 
						'can_approve_po' => 0,
						'date_format' => 0,
						'date_sep' => 0,
						'tho_sep' => 0,
						'dec_sep' => 0,
						'theme' => 'modern',
						'page_size' => 'Letter', 
						'prices_dec' => 2,
						'qty_dec' => 2,
						'rates_dec' => 4,
						'percent_dec' => 1,
						'show_gl' => 1,
						'show_codes' => 0,
						'show_hints' => 0,
						'query_size' => 10,
						'graphic_links' => 1,
						'sticky_doc_date' => 0,
						'startup_tab' => 'AP',
						'inactive' => 0,
						'allow_user' => 0
					);
			}

			$add_aria_user = $this->Users_model->insert_aria_nova_user($aria_data);

			if (isset($branches) && !empty($branches)) {
				$add_aria_branches_user = $this->Users_model->update_aria_branch_user($aria_data, '', $branches);
			}	

			# Asset Management
			$am_username = $this->input->post('am_username');
			$am_password = $this->input->post('am_password');
			$am_emp_number = $this->input->post('am_emp_number');
			$am_fname = $this->input->post('am_fname');
			$am_lname = $this->input->post('am_lname');
			$am_role = $this->input->post('am_role');
			$am_department_id = $this->input->post('am_department_id');
			$am_advance = $this->input->post('am_advance');
			$am_sub_department_id = $this->input->post('am_sub_department_id');

			if ($am_advance == 1) {
				$am_data = array(
						'username' => $am_username, 
						'password' => $am_password, 
						'emp_number' => $am_emp_number, 
						'fname' => $am_fname, 
						'lname' => $am_lname, 
						'role' => $am_role,
						'department_id' => $am_department_id,
						'email' => '',
						'inactive' => 0,
						'sub_department_id' => $am_sub_department_id
					);
			}
			else {
				$am_data = array(
						'username' => $am_username, 
						'password' => $am_password, 
						'emp_number' => $am_emp_number, 
						'fname' => $am_fname, 
						'lname' => $am_lname, 
						'role' => $am_role,
						'department_id' => $am_department_id,
						'email' => '',
						'inactive' => 0
					);
			}

			$add_am_user = $this->Users_model->insert_am_user($am_data);

			# Sales Report
			$sr_username = $this->input->post('sr_username');
			$sr_password = $this->input->post('sr_password');
			$sr_fname  = $this->input->post('sr_fname');
			$sr_lname = $this->input->post('sr_lname');

			$sr_data = array(
					'username' => $sr_username,
					'password' => $sr_password, 
					'name' => $sr_fname.' '.$sr_lname, 
					'fname' => $sr_fname,
					'lname' => $sr_lname,
					'role' => 1,
					'inactive' => 0 
				);

			$add_sr_user = $this->Users_model->insert_sr_user($sr_data);

			# Customer Maintenance

			$cm_username = $this->input->post('cm_username'); 
			$cm_password = $this->input->post('cm_password');
			$cm_fname = $this->input->post('cm_fname');
			$cm_lname = $this->input->post('cm_lname');
			$cm_branch = $this->input->post('cm_branch');
			$cm_level = $this->input->post('cm_level');

			$cm_data = array(
					'username' => $cm_username,
					'password' => $cm_password,
					'name' => $cm_fname.' '.$cm_lname,
					'fname' => $cm_fname,
					'lname' => $cm_lname,
					'role' => 1,
					'branch' => $cm_branch,
					'inactive' => 0,
					'level' => $cm_level
				);

			$add_cm_user = $this->Users_model->insert_cm_user($cm_data);

			# Purchase Order

			$po_username = $this->input->post('po_username'); 
			$po_password = $this->input->post('po_password');
			$po_fname = $this->input->post('po_fname');
			$po_lname = $this->input->post('po_lname');
			$po_emp_code = $this->input->post('po_emp_code');
			$po_role = $this->input->post('po_role');
			$po_gender = $this->input->post('po_gender');

			$po_data = array(
					'emp_code' => $po_emp_code,
					'role' => $po_role,
					'user' => $po_username,
					'password' => $po_password,
					'fname' => $po_fname,
					'lname' => $po_lname,
					'gender' => $po_gender,
					'img' => 'default.jpg',
					'theme' => 'black',
					'user_lang' => 'english',
					'online' => 0,
					'inactive' => 0

				);

			$add_po_user = $this->Users_model->insert_po_user($po_data);

			redirect('users');

		}
		else {	
			// Notice here...
		}
		// pre_r($_POST);
	}

	public function editUser() {
		// pre_r($_POST); exit;

		if (sizeof($_POST) > 0) {
			$emp_id = $this->input->post('hidden_id');

			# ARIA
			$user_id = $this->input->post('user_id');
			$password = $this->input->post('password');
			$real_name = $this->input->post('real_name');
			$phone = $this->input->post('phone');
			$email = $this->input->post('email');
			$Access = $this->input->post('Access');
			$language = $this->input->post('language');
			$pos = $this->input->post('pos');
			$profile = $this->input->post('profile');
			$rep_popup = $this->input->post('rep_popup');
			$can_approve_cv = $this->input->post('can_approve_cv');
			$can_approve_sales_remittance = $this->input->post('can_approve_sales_remittance');
			$can_approve_sdma_1 = $this->input->post('can_approve_sdma_1');
			$can_approve_sdma_2 = $this->input->post('can_approve_sdma_2');
			$is_supervisor = $this->input->post('is_supervisor');
			$can_credit_limit = $this->input->post('can_credit_limit');
			$can_negative_inv = $this->input->post('can_negative_inv');
			$can_details = $this->input->post('can_details');
			$can_void = $this->input->post('can_void');
			$can_edit = $this->input->post('can_edit');
			$can_approve_so = $this->input->post('can_approve_so');
			$can_approve_po = $this->input->post('can_approve_po');

			$branches = $this->input->post('branches');

			if (empty($pos)) 
				$pos = 1; //should be 0

			/*if (empty($profile)) 
				$profile = ''; 

			if (empty($phone)) 
				$phone = ''; 

			if (empty($email)) 
				$email = ''; */


			if (empty($can_approve_cv)) 
				$can_approve_cv = 0;

			if (empty($can_approve_sales_remittance)) 
				$can_approve_sales_remittance = 0;

			if (empty($can_approve_sdma_1)) 
				$can_approve_sdma_1 = 0;

			if (empty($can_approve_sdma_2)) 
				$can_approve_sdma_2 = 0;

			# is_supervisor settings ...

			if (empty($can_credit_limit)) 
				$can_credit_limit = 0;

			if (empty($can_negative_inv)) 
				$can_negative_inv = 0; 

			if (empty($can_details)) 
				$can_details = 0; 

			if (empty($can_void)) 
				$can_void = 0; 

			if (empty($can_edit)) 
				$can_edit = 0; 

			if (empty($can_approve_so)) 
				$can_approve_so = 0; 

			if (empty($can_approve_po)) 
				$can_approve_po = 0; 


			// $aria_data = array();
			if ($is_supervisor == 1) {
				$aria_data = array(
						'user_id' => $user_id, 
						'password' =>  $password, 
						'real_name' =>  $real_name,
						'phone' =>  $phone,
						'email' => $email,
						'role_id' => $Access,
						'language' => $language,
						'pos' => $pos,
						'print_profile' => $profile,
						'rep_popup' => $rep_popup,
						'can_approve_cv' => $can_approve_cv,
						'can_approve_sales_remittance' => $can_approve_sales_remittance,
						'can_approve_sdma_1' => $can_approve_sdma_1,
						'can_approve_sdma_2' => $can_approve_sdma_2,
						'is_supervisor' => $is_supervisor, 
						'can_credit_limit' => $can_credit_limit,
						'can_negative_inv' => $can_negative_inv,
						'can_details' => $can_details,
						'can_void' => $can_void,
						'can_edit' => $can_edit,
						'can_approve_so' => $can_approve_so, 
						'can_approve_po' => $can_approve_po,
						'date_format' => 0,
						'date_sep' => 0,
						'tho_sep' => 0,
						'dec_sep' => 0,
						'theme' => 'modern',
						'page_size' => 'Letter', 
						'prices_dec' => 2,
						'qty_dec' => 2,
						'rates_dec' => 4,
						'percent_dec' => 1,
						'show_gl' => 1,
						'show_codes' => 0,
						'show_hints' => 0,
						'query_size' => 10,
						'graphic_links' => 1,
						'sticky_doc_date' => 0,
						'startup_tab' => 'AP',
						'inactive' => 0,
						'allow_user' => 0
					);
			}
			else {
				$aria_data = array(
						'user_id' => $user_id, 
						'password' =>  $password, 
						'real_name' =>  $real_name,
						'phone' =>  $phone,
						'email' => $email,
						'role_id' => $Access,
						'language' => $language,
						'pos' => $pos,
						'print_profile' => $profile,
						'rep_popup' => $rep_popup,
						'can_approve_cv' => $can_approve_cv,
						'can_approve_sales_remittance' => $can_approve_sales_remittance,
						'can_approve_sdma_1' => $can_approve_sdma_1,
						'can_approve_sdma_2' => $can_approve_sdma_2,
						'is_supervisor' => $is_supervisor, 
						'can_credit_limit' => 0,
						'can_negative_inv' => 0,
						'can_details' => 0,
						'can_void' => 0,
						'can_edit' => 0,
						'can_approve_so' => 0, 
						'can_approve_po' => 0,
						'date_format' => 0,
						'date_sep' => 0,
						'tho_sep' => 0,
						'dec_sep' => 0,
						'theme' => 'modern',
						'page_size' => 'Letter', 
						'prices_dec' => 2,
						'qty_dec' => 2,
						'rates_dec' => 4,
						'percent_dec' => 1,
						'show_gl' => 1,
						'show_codes' => 0,
						'show_hints' => 0,
						'query_size' => 10,
						'graphic_links' => 1,
						'sticky_doc_date' => 0,
						'startup_tab' => 'AP',
						'inactive' => 0,
						'allow_user' => 0
					);
			}

			$add_aria_user = $this->Users_model->update_aria_nova_user($aria_data, $emp_id);

			if (isset($branches) && !empty($branches)) {
				$add_aria_branches_user = $this->Users_model->update_aria_branch_user($aria_data, $emp_id, $branches);
			}	

			# Asset Management
			$am_username = $this->input->post('am_username');
			$am_password = $this->input->post('am_password');
			$am_emp_number = $this->input->post('am_emp_number');
			$am_fname = $this->input->post('am_fname');
			$am_lname = $this->input->post('am_lname');
			$am_role = $this->input->post('am_role');
			$am_department_id = $this->input->post('am_department_id');
			$am_advance = $this->input->post('am_advance');
			$am_sub_department_id = $this->input->post('am_sub_department_id');

			if ($am_advance == 1) {
				$am_data = array(
						'username' => $am_username, 
						'password' => $am_password, 
						'emp_number' => $am_emp_number, 
						'fname' => $am_fname, 
						'lname' => $am_lname, 
						'role' => $am_role,
						'department_id' => $am_department_id,
						'email' => '',
						'inactive' => 0,
						'sub_department_id' => $am_sub_department_id
					);
			}
			else {
				$am_data = array(
						'username' => $am_username, 
						'password' => $am_password, 
						'emp_number' => $am_emp_number, 
						'fname' => $am_fname, 
						'lname' => $am_lname, 
						'role' => $am_role,
						'department_id' => $am_department_id,
						'email' => '',
						'inactive' => 0
					);
			}

			$add_am_user = $this->Users_model->update_am_user($am_data, $emp_id);

			# Sales Report
			$sr_username = $this->input->post('sr_username');
			$sr_password = $this->input->post('sr_password');
			$sr_fname  = $this->input->post('sr_fname');
			$sr_lname = $this->input->post('sr_lname');

			$sr_data = array(
					'username' => $sr_username,
					'password' => $sr_password, 
					'name' => $sr_fname.' '.$sr_lname, 
					'fname' => $sr_fname,
					'lname' => $sr_lname,
					'role' => 1,
					'inactive' => 0 
				);

			$add_sr_user = $this->Users_model->update_sr_user($sr_data, $emp_id);

			# Customer Maintenance

			$cm_username = $this->input->post('cm_username'); 
			$cm_password = $this->input->post('cm_password');
			$cm_fname = $this->input->post('cm_fname');
			$cm_lname = $this->input->post('cm_lname');
			$cm_branch = $this->input->post('cm_branch');
			$cm_level = $this->input->post('cm_level');

			$cm_data = array(
					'username' => $cm_username,
					'password' => $cm_password,
					'name' => $cm_fname.' '.$cm_lname,
					'fname' => $cm_fname,
					'lname' => $cm_lname,
					'role' => 1,
					'branch' => $cm_branch,
					'inactive' => 0,
					'level' => $cm_level
				);

			$add_cm_user = $this->Users_model->update_cm_user($cm_data, $emp_id);

			# Purchase Order

			$po_username = $this->input->post('po_username'); 
			$po_password = $this->input->post('po_password');
			$po_fname = $this->input->post('po_fname');
			$po_lname = $this->input->post('po_lname');
			$po_emp_code = $this->input->post('po_emp_code');
			$po_role = $this->input->post('po_role');
			$po_gender = $this->input->post('po_gender');

			$po_data = array(
					'emp_code' => $po_emp_code,
					'role' => $po_role,
					'user' => $po_username,
					'password' => $po_password,
					'fname' => $po_fname,
					'lname' => $po_lname,
					'gender' => $po_gender,
					'img' => 'default.jpg',
					'theme' => 'black',
					'user_lang' => 'english',
					'online' => 0,
					'inactive' => 0

				);

			$add_po_user = $this->Users_model->update_po_user($po_data, $emp_id);

			redirect('users');

		}
	}

	public function view_log($log_id="") {
		if (!empty($log_id)) {
			$log = $this->Users_model->get_log($log_id);
			if ($log != false) {
				echo "<pre>Log ID: ".$log['log_id']."</pre><br>";
				pre_r(json_decode($log['action_made']));
				pre_r(json_decode($log['user']));
				echo "<pre>Processed: ".$log['time_stamp']."</pre>";
			}
			else {
				echo "You have provided wrong log_id. Please try again.";
			}
		}
		else {
			echo "Please provide log_id.";
		}
	}

}




?>