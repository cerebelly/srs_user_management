<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('url', 'form'));
		$this->load->library(array('form_validation', 'session'));
		$this->load->model('login_model');
	}


	public function index() {
		if(isset($this->session->userdata['logged_in'])) {
			redirect('users');
		}
		else {
			$this->load->view('login');
		}
	}

	public function authenticate_login() {
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		if(sizeof($_POST) > 0) {
			if($this->form_validation->run() == false) {
				if(isset($this->session->userdata['logged_in'])) {
					redirect('users');
				}
				else {
					$this->load->view('login');
				}
			}	
			else {
				$userdata = array(
						'username' => $this->input->post('username'),
						'password' => do_hash($this->input->post('password'), 'md5')
					);
				$row = $this->login_model->user_exist($userdata);

				if(isset($row)) {
					if($row == true) {
						$user_row = $this->login_model->user_fetch($userdata);
						if($user_row != false) {
							$user_info = array(
									'id' => $user_row['emp_id'],
									'username' => $user_row['user_name'],
									'firstname' => $user_row['first_name'],
									'lastname' => $user_row['last_name'],
									'date_entered' => $user_row['date_entered'],
									'is_admin' => $user_row['is_admin'],
									'department_id' => $user_row['department_id'],
									'name' => $user_row['name']
								);

							$sess_info = array(
									'session_id' => md5(uniqid(rand(), TRUE)),
									'ip_address' => local_ip(),
									'user_agent' => $_SERVER['HTTP_USER_AGENT'],
									'time_stamp' => time(),
									'date_time' => date('Y-m-d H:i:s')
								);

							$excemptions = array(
								'2425', // ash
								'130',  // sir. richard quia
								'1516' // rhan
							);

							// Excemptions
							if (in_array($user_row['emp_id'], $excemptions)) {
								$this->session->set_userdata('logged_in', $user_info);
								$this->session->set_userdata('sess_info', $sess_info);
								$this->login_model->add_user_info($user_info, $sess_info);
								redirect('users');
							}
							// Check if user is_admin = Yes and department_id = 3 for ISD 
							elseif ($user_row['department_id'] == 3 && $user_row['is_admin'] == 'Yes') {
								$this->session->set_userdata('logged_in', $user_info);
								$this->session->set_userdata('sess_info', $sess_info);
								$this->login_model->add_user_info($user_info, $sess_info);
								redirect('users');
							}
							else {
								$this->data['display_message'] = array(
									'class' => 'danger',
									'header' => 'Oops!',
									'message' => 'You don\'t have access to this system, Please do reach your Administrator to make one!'
								);
								$this->load->view('login', $this->data);
							}

							

						}
					}
					else {
						$this->data['display_message'] = array(
							'class' => 'danger',
							'header' => 'Error',
							'message' => 'Invalid Username or Password!'
						);
						$this->load->view('login', $this->data);
					}
				}
			}
		}

	}	

	public function logout() {
		$sess_array = array(
			'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['display_message'] = array(
				'class' => 'info',
				'header' => 'Info',
				'message' => 'You have signed out successfully!'
			);
		$this->load->view('login', $data);
	}
}

?>