<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['dsn']      The full DSN string describe a connection to the database.
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database driver. e.g.: mysqli.
|			Currently supported:
|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Query Builder class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['encrypt']  Whether or not to use an encrypted connection.
|
|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
|
|				'ssl_key'    - Path to the private key file
|				'ssl_cert'   - Path to the public key certificate file
|				'ssl_ca'     - Path to the certificate authority file
|				'ssl_capath' - Path to a directory containing trusted CA certificats in PEM format
|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not ('mysqli' only)
|
|	['compress'] Whether or not to use client compression (MySQL only)
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
|	['failover'] array - A array with 0 or more data for connections if the main should fail.
|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
| 				NOTE: Disabling this will also effectively disable both
| 				$this->db->last_query() and profiling of DB queries.
| 				When you run a query, with this setting set to TRUE (default),
| 				CodeIgniter will store the SQL statement for debugging purposes.
| 				However, this may cause high memory usage, especially if you run
| 				a lot of SQL queries ... disable this to avoid that problem.
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $query_builder variables lets you determine whether or not to load
| the query builder class.
*/
$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.0.43',
	'username' => 'root',
	'password' => 'srsnova',
	'database' => 'orange',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

// User Management Logs 
$db['logs'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'um_logs',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

// Comment this out when centralization of aria is implemented
$db['central_aria'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'aria_pre_alpha', // To be changed to Nova Branch
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$db['asset_mngt'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.0.89',
	'username' => 'root',
	'password' => 'srsnova',
	'database' => 'asset_management',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$db['sales_report'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.0.89',
	'username' => 'root',
	'password' => 'srsnova',
	'database' => 'sales_report',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$db['cust_main'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.0.89',
	'username' => 'root',
	'password' => 'srsnova',
	'database' => 'customer_maintenance',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$db['po'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.0.89',
	'username' => 'root',
	'password' => 'srsnova',
	'database' => 'srs',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);


$my_branches = array(
		# 1. Novaliches
		'my_novaliches' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_nova'
			),
		# 2. Malabon
		'my_malabon' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_malabon'
			),
		# 2.1 SRS Kusina Malabon
		'my_kusina_malabon' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_malabon_rest'
			),
		# 3. Navotas
		'my_navotas' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_navotas'
			),
		# 4. Pavia
		'my_pavia' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_tondo'
			),
		# 5. Gagalangin
		'my_gagalangin' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_gala'
			),
		# 6. Pateros
		'my_pateros' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_pateros'
			),
		# 7. Comembo
		'my_comembo' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_comembo'
			),
		# 8. Bagong Silang
		'my_bagong_silang' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_b_silang'
			),
		# 9. Camarin
		'my_camarin' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_camarin'
			),
		# 10. Las Piñas
		'my_las_pinas' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_talon_uno'
			), 
		# 11. Imus 
		'my_imus' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_imus'
			),
		# 12. Antipolo 1
		'my_antipolo_1' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_antipolo_quezon'
			),
		# 13. Antipolo 2
		'my_antipolo_2' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_antipolo_manalo'
			),
		# 14. Cainta 1
		'my_cainta_1' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_cainta'
			),
		# 15. Cainta 2
		'my_cainta_2' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_cainta_san_juan'
			),
		# 16. Gen. T. De Leon / Valenzuela
		'my_gt_deleon' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_valenzuela'
			),
		# 17. Punturin
		'my_punturin' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_punturin_val'
			),
		# 18. San Pedro
		'my_san_pedro' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_san_pedro'
			),
		# 19. Alaminos
		'my_alaminos' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_alaminos'
			), 
		# 20. Bagumbong
		'my_bagumbong' => array(
				'hostname' => '192.168.0.91',
				'username' => 'root',
				'password' => 'srsnova',
				'database' => 'srs_aria_bagumbong'
			)

	);

foreach ($my_branches as $name => $value) {
	$db[$name] = array(
		'dsn'	=> '',
		'hostname' => $value['hostname'],
		'username' => $value['username'],
		'password' => $value['password'],
		'database' => $value['database'],
		'dbdriver' => 'mysqli',
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);
}